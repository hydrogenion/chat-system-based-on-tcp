package cn.edu.xjtu.UDP;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.nio.charset.StandardCharsets;

/**
 * @author Hydrion-QLz
 * @date 2022-06-14 15:20
 * @description
 */
public class Client1 {
    public static void main(String[] args) throws IOException {
        int serverPort = 7777;
        String serverIp = "127.0.0.1";
        DatagramSocket datagramSocket = new DatagramSocket();
        String str = "1-hhh-123";
        byte[] bs = str.getBytes(StandardCharsets.UTF_8);
        DatagramPacket datagramPacket = new DatagramPacket(bs, bs.length, new InetSocketAddress(serverIp, serverPort));
        datagramSocket.send(datagramPacket);

        byte[] receive = new byte[1024];
        DatagramPacket receivePacket = new DatagramPacket(receive, receive.length);
        datagramSocket.receive(receivePacket);
        System.out.println(new String(receivePacket.getData(), 0, receivePacket.getLength()));
    }
}
